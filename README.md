# Bomerang for KiCad Schematics

## Overview
Bomerang is a Python-based automation tool designed to streamline the management of the "Do Not Populate" (DNP) attribute in KiCad schematic files across different project variants. It utilizes the "kiutils" library to adapt schematics to meet variant-specific requirements efficiently. Symbols with variant-specific placement should have a "PNPVariant" property defined. The DNP setting is determined based on the correlation between the "PNPVariant" property and the provided variant parameters.

## Features
- Automatic DNP attribute management for KiCad schematics.
- Supports multiple project variants.
- Utilizes "kiutils" for schematic modifications.

## Installation
Ensure Python 3.x is installed on your system. Clone this repository and install the required dependencies:
```bash
git clone https://gitlab.com/CS-Mark/bomerang/
cd bomerang
pip install kiutils
```

## Usage
To use Bomerang, run the script with the desired input parameters. The basic usage is as follows:
```bash
python bomerang.py <input_path> <variant_parameters>
```

Replace <input_path> with the path to your KiCad schematic file and <variant_parameters> with the specific variant details for your project.

### Advanced Options
--output_path [OPTIONAL]: Specify the path where the modified schematic will be saved. If not specified, a "variants" subdirectory will be created in the input file directory.

## Contributing
Contributions to "Bomerang" are welcome! Please fork the repository and submit a pull request with your enhancements or bug fixes.

## License
"Bomerang" is released under the MIT License. See the LICENSE file for more details.