#!/usr/bin/env python3

"""
This software is under the MIT License.
Refer to the LICENSE file for the full text.

WARNING: This code is experimental and not guaranteed to work correctly. Anyone
who wishes to use this code is responsible for verifying the code's functionality
for their intended use. The author assumes no liability for any potential damages
incurred from its use.
"""

from kiutils.schematic import Schematic
import os
import argparse
import csv
import re
from collections import defaultdict

def debug(message):
    """Prints a debug message if debugging is enabled."""
    if DEBUG:
        print(message)

def evaluate_dnp(original_dnp, pnpvariant, pnpvariant_input):
    """
    Evaluates the DNP (Do Not Populate) status based on the PNP variant and input.
    Returns the new DNP status.
    """
    # Ensure pnpvariant and pnpvariant_input are strings
    pnpvariant_str = str(pnpvariant) if pnpvariant is not None else ''
    pnpvariant_input_str = str(pnpvariant_input) if pnpvariant_input is not None else ''

    # Split the pnpvariant and pnpvariant_input into lists for comparison
    pnpvariant_list = pnpvariant_str.split(';')
    pnpvariant_input_list = pnpvariant_input_str.split(';')

    # Normalize for case-insensitive comparison and filter out empty strings
    pnpvariant_normalized = [variant.strip().lower() for variant in pnpvariant_list if variant.strip()]
    pnpvariant_input_normalized = [variant.strip().lower() for variant in pnpvariant_input_list if variant.strip()]

    # Check conditions to determine the new dnp value
    if not pnpvariant:
        rule = 1
        new_dnp = original_dnp
    elif any("dnp" in variant for variant in pnpvariant_normalized):
        rule = 2
        new_dnp = True
    elif not set(pnpvariant_normalized).intersection(set(pnpvariant_input_normalized)):
        rule = 3
        new_dnp = True
    elif set(pnpvariant_normalized).intersection(set(pnpvariant_input_normalized)):
        rule = 4
        new_dnp = False
    else:
        # Default to the original value if none of the conditions are met
        rule = 5
        new_dnp = original_dnp
    
    if new_dnp != original_dnp:
        debug(f"rule: {rule}")
        debug(f"{pnpvariant_normalized}  {pnpvariant_input_normalized}")
    return new_dnp

def process_symbol(symbol, pnpvariant_input, symbols_for_bom, puuid, indent):
    """
    Processes a single schematic symbol, evaluates its DNP status, and adds it to the BOM if applicable.
    """
    # Default to False if attribute not present
    dnp = getattr(symbol, 'dnp', False)
    reference = "N/A"
    # Designator is evaluated from instance.path.reference or reference as fallback
    designator = "N/A"
    designator_tmp = "N/A"
    pnpvariant = ""
    value = "N/A"
    footprint = "N/A"
    designator_unit = 0
    designator_unit_tmp = 0

    # Check if the symbol has a properties attribute and iterate through properties
    if hasattr(symbol, 'properties'):
        for prop in symbol.properties:
            if prop.key == "Reference":
                reference = prop.value
            if prop.key == "PNPVariant":
                pnpvariant = prop.value
            if prop.key == "Value":
                value = prop.value
            if prop.key == "Footprint":
                footprint = prop.value
    match_count = 0
    if hasattr(symbol, 'instances'):
        for instance in symbol.instances:
            paths_list = list(instance.paths)
            total_paths = len(paths_list)
            for iteration, path in enumerate(instance.paths):
                if puuid == path.sheetInstancePath:
                    match_count += 1
                    designator_tmp = path.reference
                    designator_unit_tmp = path.unit
    else:
        debug("Does not have symbol.instances, use reference as designator fallback")
        designator = reference
        designator_unit = symbol.unit
    if match_count == 1:
        designator = designator_tmp
        designator_unit = designator_unit_tmp
    else:
        raise ValueError(f"Evaluation of designator for symbol: {reference} failed, count: {match_count} is not 1")

    # Evaluate the new dnp value
    original_dnp = dnp
    dnp = evaluate_dnp(original_dnp, pnpvariant, pnpvariant_input)
    setattr(symbol, 'dnp', dnp)

    # Add attribut designator into symbol
    setattr(symbol, 'designator', designator)

    # Add attribut designator_unit into symbol
    setattr(symbol, 'designator_unit', designator_unit)

    # Key for grouping
    key = (value, footprint)
    # Add to BOM
    symbols_for_bom[key].append(symbol)

    # Print the symbol information along with its reference/designator
    if dnp != original_dnp:
        debug(f"{'  '*(indent+1)}- Symbol: {symbol.libId}, Designator: {reference}, "
            f"PNPVariant: {pnpvariant}, "
            f"orig_DNP: {original_dnp}, DNP: {dnp}")
 
def process_schematic(path, pnpvariant_input, output_dir, symbols_for_bom, puuid='', indent=0):
    """
    Processes a schematic file, including all its symbols and sub-sheets, and saves a new version.
    """
    # Load the existing schematic
    schematic = Schematic.from_file(path, ENCODING)

    # Print the current sheet name
    debug(f"{'  '*indent}Schematic: {os.path.basename(path)}")

    # Check if it is on TOP level, possible use indent = 0
    if puuid == '':
        if hasattr(schematic, 'uuid'):
            uuid = schematic.uuid
            debug(f"TOP level uuid: {uuid}")
            puuid += '/' + uuid
    else:
        debug(f"Sheet path uuid: {puuid}")

    # List all symbols in the current schematic
    if hasattr(schematic, 'schematicSymbols') and schematic.schematicSymbols:
        for symbol in schematic.schematicSymbols:
            process_symbol(symbol, pnpvariant_input, symbols_for_bom, puuid, indent)

    # Process sub-sheets
    if hasattr(schematic, 'sheets') and schematic.sheets:
        for sheet in schematic.sheets:
            sub_sheet_path = os.path.join(os.path.dirname(path), sheet.fileName.value)
            debug(f"{'  '*(indent+1)}Sub-sheet: {sheet.sheetName.value} - {sheet.fileName.value}")
            if hasattr(schematic, 'uuid'):
                sub_uuid = sheet.uuid
                debug(f"Sub sheet uuid: {sub_uuid}")
            # Recursive call here
            process_schematic(sub_sheet_path, pnpvariant_input, output_dir, symbols_for_bom,
                  puuid + '/' + sub_uuid, indent + 2)
    else:
        debug(f"{'  '*(indent+1)}- (No further subsheets)")

    # After processing all symbols and sub-schematics
    output_path = os.path.join(output_dir, os.path.basename(path))
    schematic.to_file(output_path, ENCODING)

def group_sort_key(group):
    """
    Returns a sorting key based on the priority of the group prefix.
    Priority is defined for specific prefixes, with all others following in alphabetical order.
    """
    # Retrieve the prefix of the first designator in the group to determine the group's category
    _, symbols = group
    if symbols:
        # Assuming 'symbols' is a list of dictionaries or objects with a 'designator' attribute
        first_designator = getattr(symbols[0], 'designator', "N/A")
        # Attempt to extract the prefix from the first designator
        prefix_match = re.match(r"([a-zA-Z]+)", first_designator)
        if prefix_match:
            # Extract the prefix
            prefix = prefix_match.group(1)
            # Define explicit priority for known prefixes
            prefix_priority = {'Bat': 1, 'C': 2, 'D':3, 'F':4, 'H':5, 'K':6, 'L':7, 'LP':8, 'Q':9, 'R': 10}
            # Return a tuple of (priority, prefix), unknown prefixes are sorted alphabetically after known ones
            return (prefix_priority.get(prefix, 99), prefix)
    # Default case, if no designator exists or it doesn't have a recognizable prefix
    return (99, '')  

def designator_sort_key(designator):
    """
    Returns a sorting key: the first part is the textual prefix, including non-alphanumeric characters,
    the second part is the numerical value.
    """  
    match = re.match(r"([^0-9]*)(\d+)", designator)
    if match:
        prefix, number = match.groups()
        return (prefix, int(number))
    else:
        # Returns the original designator with high number if it doesn't match the pattern
        return (designator, float('inf'))

def main():
    """
    Main function that parses command line arguments and initiates the processing of the KiCad schematic file.
    """
    global ENCODING
    global DEBUG

    ENCODING = "utf-8"
    DEBUG = False

    # Parser for command line arguments
    parser = argparse.ArgumentParser(description=(
    'Automate the management of the "Do Not Populate" (DNP) attribute in KiCad schematic files. '
    'Symbols with variant-specific placement should have a "PNPVariant" property defined. '
    'The DNP setting is determined based on the correlation between the "PNPVariant" property '
    'and the provided "variant_parameters".'))

    parser.add_argument('input_path', help='The path to the KiCad schematic file.')
    parser.add_argument(
    'variant_parameters',
    help=('The specific variant details for your project, separated by semicolons.'))

    parser.add_argument(
    '--output_path',
    help=('Optional: The path where the modified schematic will be saved. '
          'If not specified, a "variants" subdirectory will be created in the '
          'input file directory.'),
    nargs='?', default=None)

    args = parser.parse_args()

    input_path = args.input_path
    pnpvariant_input = args.variant_parameters
    output_path_user = args.output_path

    # Specify the output directory
    if output_path_user:
        output_dir = output_path_user
    else:
        output_dir = os.path.join(os.path.dirname(input_path), 'variants')

    csv_file_path = os.path.join(output_dir, 'bom_output.csv')
    
    # Create output directory if not exists
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Initialize a structure for storing and grouping symbols
    symbols_for_bom = defaultdict(list)

    # Start processing from the top-level schematic
    process_schematic(input_path, pnpvariant_input, output_dir, symbols_for_bom)

    # Sort groups of symbols according to their prefix
    sorted_symbol_groups = sorted(symbols_for_bom.items(), key=lambda group: group_sort_key(group))

    with open(csv_file_path, mode='w', newline='', encoding='utf-8') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        debug("BOM output here:")
        csv_writer.writerow(['Designator', 'Footprint', 'Quantity', 'Value'])
        for index, ((value, footprint), symbols) in enumerate(sorted_symbol_groups):
            designators = []
            # Compilation of a list of unique references for each group
            for symbol in symbols:
                designator = "N/A"
                designator_unit = 0
                in_bom = getattr(symbol, 'inBom')
                on_board = getattr(symbol, 'onBoard')
                dnp = getattr(symbol, 'dnp')
                designator = getattr(symbol, 'designator')
                designator_unit = getattr(symbol, 'designator_unit')
                if designator_unit != 1 or dnp or not in_bom or not on_board or designator.startswith(("#PWR", "#FLG")):
                    debug("Skip {}, unit: {}, dnp: {}, in_bom: {}, on_board: {}".format(
                        designator, designator_unit, dnp, in_bom, on_board))
                    continue
                designators.append(designator)
            # Sort designators if the list is not empty
            if designators:
                quantity = len(designators)
                sorted_designators_str = ", ".join(sorted([designator for designator in designators], key=designator_sort_key))
                debug(f'Value: {value}, Footprint: {footprint}, Quantity: {quantity}, Designators: "{sorted_designators_str}"')
                # Remove library prefix from the footprint
                cleaned_footprint = footprint.split(':', 1)[-1] if ':' in footprint else footprint
                csv_writer.writerow([sorted_designators_str, cleaned_footprint, len(designators), value])
        print(f"BOM output has been saved to: {csv_file_path}")

if __name__ == '__main__':
    main()
